# 電話簿
<!-- 0973663160 噓~不可以偷偷告訴別人 -->
嗨！ 
  
以下是一些可以聯繫上我的方式，因為打算不再使用社群軟體了(Facebook, Instagram...)。_下一個就是你了LINE，我正在看著你。_
  
之前有想要架一個網站，不過擔心發懶就沒時間維護了，所以最後決定至少先把聯絡方式放上來，如果未來有新進展的話...那就再看看吧？

## 教學 
_updated:22/04/30_ 

最近會陸陸續續將xmpp跟email的教學更新到 _"tutorial"_ 資料夾

## XMPP 
  
`pisutooq@xmpp.is`

OMEMO key
  
`Laptop: d09de4b7 71a5acfe 331894ba eabcc33e  284671c3 430903f1  e8e72451 c1797806`
  
`Cellphone: 8f7b2eeb 028b4923 89552129 bfc962d5 67d15697 b9963d80 e8031134 cea93921`

## ~~Email~~
_**比較不推**_ 的聯繫方式。
  
Email:marklin0913@protonmail.com (私人) [(PGP)](./key/publickey.marklin0913@protonmail.com.asc)
  
Fingerprint:`ba450b28ceeb4af0e73feed7e761119731dd1636`
  
Email:411054033@gms.ndhu.edu.tw（學校）[(GPG)](./key/publickey.411054033@gms.ndhu.edu.tw.asc)
  
Fingerprint:`698A9D3DE92A79931EC70F2B37194D92969AC145`
  
喔對，你還是可以用我的公鑰加密東東然後寄給我啦。這個我就沒話說了
  
## 拒絕再加入新聯絡人的軟體
- LINE 