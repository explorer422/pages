# [xmpp 教學] Android 篇

## 註冊 
你可以自己上網找任何有提供xmpp的公開伺服器，這邊示範如何用 `anonym.im` 註冊帳號，並大略的帶你走過一趟如何使用xmpp。

## 下載F-Droid
F-Droid 有點像是Apple 生態系裡面的App Store、Android生態系裡面的Google Play Store，**!!不過!!** F-Droid上面應用程式的程式碼都是完全公開的(包含F-Droid本身也是)，也就是說

[F-Droid 網頁](https://f-droid.org/)

[F-Droid 下載連結](https://f-droid.org/F-Droid.apk)

## 下載 Conversations[^1]
搜尋應用程式 > Conversations > 下載

## 加入新聯絡人
`Conversations 主畫面 > 右下角訊息按鈕 > 右下角加號 > Add Contact > 輸入聯絡人的xmpp ID`
## [重要]OMEMO加密
### 開啟加密(強制)
`Conversations > 右上角的功能選單 > 設定(Settings) > Privacy > OMEMO Encryption > **Always** `

###　核對鑰匙
接下來這個步驟很重要，要確保你拿到的鑰匙是真的[^2]

` 打開聊天室 > 右上角的功能點點 > Contact Details > 底下的一大串的OMEMO鑰匙`

這邊就是控制哪些鑰匙是用來加解密訊息的，未來倘若聯絡人不使用某支鑰匙時，就可以從這邊把那支鑰匙關掉。因為OMEMO加密系統支援多裝置，所以像我就會有兩個OMEMO鑰匙，因為我在筆電跟手機上面都有使用。

`Laptop: d09de4b7 71a5acfe 331894ba eabcc33e  284671c3 430903f1  e8e72451 c1797806`
  
`Cellphone: 8f7b2eeb 028b4923 89552129 bfc962d5 67d15697 b9963d80 e8031134 cea93921`

設定好之後，每當你的聯絡人支援跟你相同的加密系統的時候，聊天室的訊息就會出現鑰匙鎖的符號以及盾牌裡面有個勾勾，這就表示你的加密是成功的。

## 結語
讀到這裡表示你已經可以使用xmpp跟其他人聯繫了，給自己一點鼓勵吧。如果還有什麼沒有搞得很懂得歡迎再聯絡我，或是在這個repo裡面開issue也是可以(我會很開心的，哈哈)。最後，歡迎加入xmpp大家庭!!

[^1]: 不在Google Play Store 上面下載的原因是因為從Play Store 好像要額外付費。不過因為Source Code 是公開的，所以。簡單來說就是懂一點點額外的知識可以讓你用到不錯的東西，同時又可以省錢。
[^2]: 有些情況是，你拿到的鑰匙是別人從傳輸過程中偷偷放進去或單純毀損的，所以當你以為收到的是A的鑰匙的時候，說不定拿到的是B的鑰匙。而在這種情況下，B就可以輕輕鬆鬆的解開你原先要傳給A的訊息了。 這種叫做Man-In-The-Middle Attack(MITM) [更多資訊: EFF網頁介紹MITM ](https://ssd.eff.org/en/glossary/man-middle-attack) 